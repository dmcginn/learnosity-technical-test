/*global LearnosityAmd*/
LearnosityAmd.define(['jquery-v1.10.2'], function ($) {
    'use strict';

    //function to build up HTML table and add onclick handlers - return either as JQuery Element or String of HTML
    function buildHtmlAndInteractions(init) {

        var $htmlObj;

        //
        //Learnosity Tech Test: Complete here
        //

        //Build up the structure you want to use here, and add any event handling you may want to use
        $htmlObj = $('<div class="clock-container"> <span class="evening-indicator"> AM </span><span class="evening-indicator"> AM </span> <article class="clock"> <div class="hours-container"> <div class="hours"></div> </div> <div class="minutes-container"> <div class="minutes"></div> </div> </article> </div><input id="question_input" type="text" placeholder="e.g. 15:45" />');

        //Split valid_response (String) into hours and minutes
        var timeObj = parseValidResponse(init.question.valid_response);

        //Set the display of the clock based on the valid_response value, which was parsed into timeObj
        initClock($htmlObj, timeObj);

        return $htmlObj;
    }

    //function to change UI based on correct or incorrect answer status
    function addValidationUI(questionAnswerStatus) {

        //
        //Learnosity Tech Test: Complete here
        //
        var inputField = $('#question_input');

        //Reset valid/invalid classes each time validation UI is updated
        inputField.removeClass('invalid valid');

        //Add valid or invalid class depending on questionAnswerStatus
        if (questionAnswerStatus == false) {
            inputField.addClass('invalid');
        } else {
            inputField.addClass('valid');
        }
    }

    //function to parse time string (e.g. '20:45') and return objects for
    //each hand, including their integer value and angle (for clock display)
    function parseValidResponse(timeString) {

        //Split String into Array, e.g. "12:45" => ["12", "45"]
        var parsedTime = timeString.split(":");

        //Cast Strings as Integers for use in calculations
        var hours = parseInt(parsedTime[0]);
        var minutes = parseInt(parsedTime[1]);

        //Hours and minute hands objects.
        //Rotation angle calculated for display on clock face.
        var hands = [
          {
            hand: 'hours',
            value: hours,
            angle: (hours * 30) + (minutes / 2)
          },
          {
            hand: 'minutes',
            value: minutes,
            angle: (minutes * 6)
          }
        ];

        return hands;
    }

    //function to update clock display.
    //Updates CSS to rotate hands, and changes AM/PM indicator text depending on time.
    function initClock(html, time) {

        //Select elements
        var hoursEl = html.find('.hours');
        var minutesEl = html.find('.minutes');

        //Update CSS to rotate hands in different browsers
        hoursEl.css({transform: 'rotateZ(' + time[0].angle + 'deg)', webkitTransform: 'rotateZ(' + time[0].angle + 'deg)'});
        minutesEl.css({transform: 'rotateZ(' + time[1].angle + 'deg)', webkitTransform: 'rotateZ(' + time[1].angle + 'deg)'});

        //Change AM/PM indicator to display PM if hour >= 12
        if (time[0].value >= 12) {
            html.find('.evening-indicator').text('PM');
        }
    }


    //Learnosity:
    //Demo framework for custom question starts here. 
    //You don't have to change anything, except for the "isValid() function
    //but you can if yo see something useful.


    function CustomNumberPad(init) {

        //create example table and button elements for constructing numberpad.
        var $questionTypeHtml = buildHtmlAndInteractions(init);
        init.$el.html($questionTypeHtml);

        //Reset input field valid/invalid classes on focus
        $('#question_input').on('focus', function() {
            $(this).removeClass('invalid valid');
        });

        //add on validate button.
        init.events.on('validate', function () {
            //check if answer is correct, and pass true or false to the function to update validation UI
            var scorer = new CustomNumberPadScorer(init.question, init.response);

            addValidationUI(scorer.isValid());
        });

        init.events.trigger('ready');
    }

    function CustomNumberPadScorer(question, response) {
        this.question = question;
        this.response = response;
    }


    CustomNumberPadScorer.prototype.isValid = function () {
        //return true or false depending on student answer

        //
        //Learnosity Tech Test: Complete here
        //

        //Check whether the question_input value is equal to valid_response
        return $('#question_input').val() === this.question.valid_response;
    };

    CustomNumberPadScorer.prototype.score = function () {
        return this.isValid() ? this.maxScore() : 0;
    };

    CustomNumberPadScorer.prototype.maxScore = function () {
        return this.question.score || 1;
    };

    return {
        Question: CustomNumberPad,
        Scorer:   CustomNumberPadScorer
    };
});
